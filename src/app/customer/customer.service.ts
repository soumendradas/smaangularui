import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Customer } from './customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private customer_url:string = `${environment.base_url}/customer`;
  constructor(private http: HttpClient) { }

  getAllCustomer(): Observable<Customer[]>{
    return this.http.get<Customer[]>(`${this.customer_url}/all`);
  }

  saveCustomer(customer: Customer): Observable<any>{
    return this.http.post(`${this.customer_url}/save`, customer, {responseType: 'text'});
  }
  
}
