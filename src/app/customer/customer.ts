export class Customer {

    constructor(public id: number,
                public name: string,
                public email: string,
                public imagePath: string,
                public mobile: string,
                public pancard: string,
                public aadharId: number,
                public gender: string) {}
}
