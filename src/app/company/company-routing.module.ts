import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyAllComponent } from './company-all/company-all.component';

const routes: Routes = [
  {path: "", 
  children:[
    {path:"", component: CompanyAllComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
