import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'company', 
      loadChildren: ()=> import('./company/company.module').then(m=>m.CompanyModule)},
  {path: 'customer',
      loadChildren: ()=> import('./customer/customer.module').then(m=>m.CustomerModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
