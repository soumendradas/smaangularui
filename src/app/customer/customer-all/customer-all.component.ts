import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-all',
  templateUrl: './customer-all.component.html',
  styleUrls: ['./customer-all.component.css']
})
export class CustomerAllComponent implements OnInit {

  customers: Customer[] = [];
  message: string = '';
 
  constructor(private service: CustomerService) { }

  ngOnInit(): void {
    this.getAllCustomers();
    
  }

  getAllCustomers(){
    this.service.getAllCustomer().subscribe({
      next: data=> this.customers = data,
      error: error=> this.message = error
    });
  }

}

