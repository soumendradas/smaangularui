import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-save',
  templateUrl: './customer-save.component.html',
  styleUrls: ['./customer-save.component.css']
})
export class CustomerSaveComponent implements OnInit {

  customer:Customer = new Customer(0, "", "", "", "", "", 0, "");
  message:any;

  constructor(private service: CustomerService) { }

  ngOnInit(): void {
  }

  createCustomer(){
    this.service.saveCustomer(this.customer).subscribe({
      next: data=> {this.message= data;
                    this.customer = new Customer(0, "", "", "", "", "", 0, "")},
      error: error=> console.log(error)
    })
  }

}
