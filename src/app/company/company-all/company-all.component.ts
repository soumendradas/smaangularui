import { Component, OnInit } from '@angular/core';
import { Company } from '../company';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-company-all',
  templateUrl: './company-all.component.html',
  styleUrls: ['./company-all.component.css']
})
export class CompanyAllComponent implements OnInit {

  companies: Company[] = [];
  constructor(private service: CompanyService) { }
  
  ngOnInit(): void {
    this.getAllCompany();
  }
  

  getAllCompany() {
    this.service.getAllCompany().subscribe({
      next: (data) =>{this.companies = data;
                      console.log(data);
                      console.log(this.companies)},
      error: (e) =>{console.log(e);}
    });

    // console.log(this.companies);
  }

}
