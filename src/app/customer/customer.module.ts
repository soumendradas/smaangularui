import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerAllComponent } from './customer-all/customer-all.component';
import { CustomerSaveComponent } from './customer-save/customer-save.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CustomerAllComponent,
    CustomerSaveComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    HttpClientModule,
    FormsModule,
  ]
})
export class CustomerModule { 
  constructor(){
    console.log("Customer module initialized");
  }
}
