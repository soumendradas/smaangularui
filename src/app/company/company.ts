import { CompanyTypes } from "./company-types";
import { Operates } from "./operates";
import { ServiceCode } from "./service-code";
import {Address} from "../common-model/address"

export class Company {

    constructor(public id:number,
                public name:string,
                public comRegId:string,
                public comType: CompanyTypes,
                public parentOrg: string,
                public modeOfOperate: Operates,
                public serviceCode: ServiceCode,
                public Address: Address) {}
}
