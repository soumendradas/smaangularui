import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerAllComponent } from './customer-all/customer-all.component';
import { CustomerSaveComponent } from './customer-save/customer-save.component';

const routes: Routes = [
  {path: "", 
    children:[
      {path:"", component: CustomerAllComponent},
      {path:"create", component: CustomerSaveComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
