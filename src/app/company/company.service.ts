import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Company } from './company';
import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  company_url = `${environment.base_url}/company`;

  getAllCompany(): Observable<Company[]>{

    return this.http.get<Company[]>(`${this.company_url}/all`);
  }
}
