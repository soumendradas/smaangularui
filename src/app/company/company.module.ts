import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyAllComponent } from './company-all/company-all.component';


@NgModule({
  declarations: [
    CompanyAllComponent
  ],
  imports: [
    HttpClientModule,
    CompanyRoutingModule, 
    
  ]
})
export class CompanyModule {
  constructor(){
    console.log("Company module initialized")
  }
 }
